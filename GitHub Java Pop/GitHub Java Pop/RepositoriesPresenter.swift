//
//  RepositoriesPresenter.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 6/27/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import Foundation

class RepositoriesPresenter: RepositoriesPresenterProtocol {
    
    weak var delegate: RepositoriesViewDelegate?
    
    private var repositories: [Repository] = []
    private var currentPage: Int = 1
    private let service: RepositoryService
    
    init(service: RepositoryService) {
        self.service = service
    }
    
    var numberOfRepositories: Int {
        return repositories.count
    }
    
    func refreshRepositories() {
        currentPage = 1
        repositories = []
        delegate?.refreshRepositories()
        fetchNextRepositoriesPage()
    }
    
    func fetchNextRepositoriesPage() {
        delegate?.setLoading(true)
        service.fetchRepositories(for: currentPage, response: (success: { repositoriesResponse in
            let repositories = repositoriesResponse.repositories
            let newRepositoriesRange = self.repositories.endIndex..<(self.repositories.endIndex + repositories.endIndex)
            self.currentPage += 1
            self.repositories += repositories
            self.delegate?.setEmpty(self.repositories.isEmpty)
            self.delegate?.insertRepositories(inRange: newRepositoriesRange)
        }, failure: { error in
            self.delegate?.display(error: error)
        }, completion: {
            self.delegate?.setLoading(false)
        }))
    }
    
    func repository(atIndex index: Int) -> RepositoryDataView {
        return RepositoryDataView(repository: repositories[index])
    }
    
    func didSelectRepository(atIndex index: Int) {
        let repository = repositories[index]
        if let url = repository.pullRequestUrl?.asURL() {
            delegate?.showRepositoryPullRequests(fromURL: url)
        }
    }
    
    func pullRequestsURLForRepository(at index: Int) -> URL? {
        return repositories[index].pullRequestUrl?.asURL()
    }
    
    
}
