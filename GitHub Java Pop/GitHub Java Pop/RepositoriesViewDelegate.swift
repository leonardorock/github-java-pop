//
//  RepositoriesViewProtocol.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 6/27/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import Foundation

protocol RepositoriesViewDelegate: class {
    var isLoading: Bool { get set }
    var isEmpty: Bool { get set }
    func setLoading(_ loading: Bool)
    func setEmpty(_ empty: Bool)
    func display(error: Error)
    func insertRepositories(inRange range: CountableRange<Int>)
    func refreshRepositories()
    func showRepositoryPullRequests(fromURL url: URL)
}
