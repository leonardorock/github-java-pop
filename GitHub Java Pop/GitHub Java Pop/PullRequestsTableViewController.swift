//
//  PullRequestsTableViewController.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 6/27/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import UIKit
import SafariServices
import DZNEmptyDataSet

class PullRequestsTableViewController: UITableViewController, PullRequestsViewDelegate, DZNEmptyDataSetSource, UIViewControllerPreviewingDelegate {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var presenter: PullRequestsPresenterProtocol!
    var repositoryPullRequests: URL?
    var isLoading: Bool = false
    var isEmpty: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = PullRequestsPresenter(service: PullRequestService())
        presenter.delegate = self
        presenter.fetchNextPullRequestsPage()
        setupTableView()
        
        if #available(iOS 9.0, *), traitCollection.forceTouchCapability == .available {
            registerForPreviewing(with: self, sourceView: view)
        }
    }
    
    func setupTableView() {
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshControlValueChanged), for: .valueChanged)
        tableView.emptyDataSetSource = self
        tableView.register(cellType: PullRequestTableViewCell.self)
    }
    
    @objc func refreshControlValueChanged() {
        presenter.refreshPullRequests()
    }
    
    // MARK: - Pull requests view protocol
    
    func setLoading(_ loading: Bool) {
        isLoading = loading
        refreshControl?.endRefreshing()
        if loading {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }
    
    func setEmpty(_ empty: Bool) {
        isEmpty = empty
        tableView.reloadEmptyDataSet()
    }
    
    func display(error: Error) {
        let alert = UIAlertController(title: NSLocalizedString("Error", comment: "Error title"), message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(.ok)
        present(alert, animated: true, completion: nil)
    }
    
    func insertPullRequests(inRange range: CountableRange<Int>) {
        let newIndexPaths = range.map { IndexPath(row: $0, section: 0) }
        tableView.insertRows(at: newIndexPaths, with: .automatic)
    }
    
    func refreshPullRequests() {
        tableView.reloadData()
    }
    
    func repositoryPullRequestsURL() -> URL? {
        return repositoryPullRequests
    }
    
    func openPullRequestPage(withURL url: URL) {
        if #available(iOS 9.0, *) {
            let safariViewController = SFSafariViewController(url: url)
            present(safariViewController, animated: true, completion: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfPullRequests
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: PullRequestTableViewCell.self)
        cell.setup(with: presenter.pullRequest(atIndex: indexPath.row))
        return cell
    }
    
    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectPullRequest(atIndex: indexPath.row)
    }
    
    // MARK: - Scroll view delegate
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let reachedTheBottom = scrollView.contentOffset.y + scrollView.frame.size.height > scrollView.contentSize.height
        if reachedTheBottom && !isLoading {
            presenter.fetchNextPullRequestsPage()
        }
    }
    
    // MARK: - Empty data source
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let title: String
        if let _ = repositoryPullRequests {
            title = isEmpty ? NSLocalizedString("No Pull Request found", comment: "No pull requests found message") : ""
        } else {
            title = NSLocalizedString("Select a repository to see it's pull requests", comment: "No repository selected message")
        }
        return NSAttributedString(string: title)
    }
    
    // MARK: - View controller previewing delegate
    
    @available(iOS 9.0, *)
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let indexPath = tableView.indexPathForRow(at: location), let sourceRect = tableView.cellForRow(at: indexPath)?.frame, let url = presenter.pullRequestURL(at: indexPath.row) else {
            return nil
        }
        previewingContext.sourceRect = sourceRect
        return SFSafariViewController(url: url)
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        present(viewControllerToCommit, animated: true, completion: nil)
    }
    
}
