//
//  PullRequest.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 6/28/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import Foundation

struct PullRequest: Codable {
    
    var title: String?
    var subject: String?
    var user: User?
    var date: Date?
    var url: String?
    var state: State?
    
    enum CodingKeys: String, CodingKey {
        case title,
        subject = "body",
        user,
        date = "created_at",
        url = "html_url",
        state
    }
    
}

extension PullRequest {
    enum State: String, Codable {
        case open = "open"
        case closed = "closed"
    }
}
