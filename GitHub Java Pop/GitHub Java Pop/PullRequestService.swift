//
//  PullRequestService.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 6/28/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import Foundation

class PullRequestService: Service {
    
    func fetchPullRequests(at url: URL, page: Int, response: ModelResponse<[PullRequest]>) {
        var parameters: [String : Any] = [:]
        parameters["state"] = "all"
        parameters["page"] = page
        request(url: url, parameters: parameters, response: response)
    }
    
}

