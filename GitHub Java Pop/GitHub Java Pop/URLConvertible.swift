//
//  URLConvertible.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 1/20/18.
//  Copyright © 2018 Leonardo Oliveira. All rights reserved.
//

import Foundation

protocol URLConvertible {
    func asURL() -> URL?
}

extension String: URLConvertible {
    func asURL() -> URL? {
        var str = self
        if let invalidChar = str.range(of: "{") {
            str = String(str[..<invalidChar.lowerBound])
        }
        return URL(string: str)
    }
}

extension URL: URLConvertible {
    func asURL() -> URL? {
        return self
    }
}
