//
//  SeviceError.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 1/20/18.
//  Copyright © 2018 Leonardo Oliveira. All rights reserved.
//

import Foundation

struct ServiceError: LocalizedError {
    let statusCode: Int
    var errorDescription: String? {
        return HTTPURLResponse.localizedString(forStatusCode: statusCode)
    }
    var data: Data?
}

