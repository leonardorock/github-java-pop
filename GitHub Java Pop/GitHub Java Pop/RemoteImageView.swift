//
//  RemoteImageView.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 1/19/18.
//  Copyright © 2018 Leonardo Oliveira. All rights reserved.
//

import UIKit

class RemoteImageView: UIImageView {
    
    private var downloadTask: URLSessionDataTask? = nil
    
    private lazy var session: URLSession = { () -> URLSession in
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.requestCachePolicy = .returnCacheDataElseLoad
        return URLSession(configuration: sessionConfiguration)
    }()
    
    func setImage(withURL url: URL?, placeholderImage: UIImage?) {
        cancelImageRequest()
        self.image = placeholderImage
        guard let url = url else { return }
        downloadTask = session.dataTask(with: url) { [weak self] (data, response, error) in
            guard let data = data else { return }
            DispatchQueue.main.async {
                self?.image = UIImage(data: data)
            }
        }
        downloadTask?.resume()
    }
    
    func cancelImageRequest() {
        downloadTask?.cancel()
        downloadTask = nil
    }

}
