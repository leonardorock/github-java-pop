//
//  RepositoriesPresenterProtocol.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 6/27/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import Foundation

protocol RepositoriesPresenterProtocol {
    weak var delegate: RepositoriesViewDelegate? { get set }
    var numberOfRepositories: Int { get }
    func refreshRepositories()
    func fetchNextRepositoriesPage()
    func repository(atIndex index: Int) -> RepositoryDataView
    func didSelectRepository(atIndex index: Int)
    func pullRequestsURLForRepository(at index: Int) -> URL?
}
