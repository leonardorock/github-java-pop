//
//  PullRequestTableViewCell.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 1/20/18.
//  Copyright © 2018 Leonardo Oliveira. All rights reserved.
//

import UIKit
import Reusable

final class PullRequestTableViewCell: UITableViewCell, NibReusable {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var ownerPhotoImageView: RemoteImageView!
    @IBOutlet weak var ownerUsernameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var statusBackgroundView: UIView!
    
    func setup(with pullRequest: PullRequestDataView) {
        titleLabel.text = pullRequest.title
        subjectLabel.text = pullRequest.subject
        statusLabel.text = pullRequest.state
        ownerUsernameLabel.text = pullRequest.username
        dateLabel.text = pullRequest.date
        ownerPhotoImageView.setImage(withURL: pullRequest.avatarURL, placeholderImage: #imageLiteral(resourceName: "Photo Outline 32h"))
        statusBackgroundView.backgroundColor = pullRequest.stateColor
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        ownerPhotoImageView.cancelImageRequest()
    }
    
}
