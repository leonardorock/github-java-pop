//
//  UIAlertActionExtension.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 6/28/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import UIKit

extension UIAlertAction {
    static let ok = UIAlertAction(title: NSLocalizedString("OK", comment: "OK title button"), style: .default, handler: nil)
}
