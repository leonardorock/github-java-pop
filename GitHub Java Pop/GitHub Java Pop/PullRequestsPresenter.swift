//
//  PullRequestsPresenter.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 6/28/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import Foundation

class PullRequestsPresenter: PullRequestsPresenterProtocol {
    
    weak var delegate: PullRequestsViewDelegate?
    
    private var pullRequests: [PullRequest] = []
    private var currentPage: Int = 1
    private let service: PullRequestService
    
    init(service: PullRequestService) {
        self.service = service
    }
    
    var numberOfPullRequests: Int {
        return pullRequests.count
    }
    
    func refreshPullRequests() {
        currentPage = 1
        pullRequests = []
        delegate?.refreshPullRequests()
        fetchNextPullRequestsPage()
    }
    
    func fetchNextPullRequestsPage() {
        guard let url = delegate?.repositoryPullRequestsURL() else { return }
        delegate?.setLoading(true)
        service.fetchPullRequests(at: url, page: currentPage, response: (success: { pullRequests in
            let newPullRequestsRange = self.pullRequests.endIndex..<(self.pullRequests.endIndex + pullRequests.endIndex)
            self.currentPage += 1
            self.pullRequests += pullRequests
            self.delegate?.setEmpty(self.pullRequests.isEmpty)
            self.delegate?.insertPullRequests(inRange: newPullRequestsRange)
        }, failure: { error in
            self.delegate?.display(error: error)
        }, completion: {
            self.delegate?.setLoading(false)
        }))
    }
    
    func pullRequest(atIndex index: Int) -> PullRequestDataView {
        return PullRequestDataView(pullRequest: pullRequests[index])
    }
    
    func didSelectPullRequest(atIndex index: Int) {
        let pullRequest = pullRequests[index]
        if let url = pullRequest.url?.asURL() {
            delegate?.openPullRequestPage(withURL: url)
        }
    }
    
    func pullRequestURL(at index: Int) -> URL? {
        return pullRequests[index].url?.asURL()
    }
    
}
