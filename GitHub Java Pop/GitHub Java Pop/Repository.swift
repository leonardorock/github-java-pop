//
//  Repository.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 6/27/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import Foundation

struct RepositoriesResponse: Codable {
    
    var numberOfRepositories: Int
    var incompleteResults: Bool
    var repositories: [Repository]
    
    enum CodingKeys: String, CodingKey {
        case numberOfRepositories = "total_count",
        incompleteResults = "incomplete_results",
        repositories = "items"
    }
    
}

struct Repository: Codable {
    
    var name: String?
    var description: String?
    var numberOfForks: Int
    var numberOfStars: Int
    var owner: User
    var pullRequestUrl: String?
    
    enum CodingKeys: String, CodingKey {
        case name,
        description,
        numberOfForks = "forks_count",
        numberOfStars = "stargazers_count",
        owner,
        pullRequestUrl = "pulls_url"
    }
}

