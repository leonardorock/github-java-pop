//
//  RepositoriesTableViewController.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 6/27/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class RepositoriesTableViewController: UITableViewController, RepositoriesViewDelegate, DZNEmptyDataSetSource, UIViewControllerPreviewingDelegate {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var isLoading: Bool = false
    var isEmpty: Bool = false
    var presenter: RepositoriesPresenterProtocol!
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = RepositoriesPresenter(service: RepositoryService())
        presenter.delegate = self
        presenter.fetchNextRepositoriesPage()
        setupTableView()
        
        if #available(iOS 9.0, *), traitCollection.forceTouchCapability == .available {
            registerForPreviewing(with: self, sourceView: view)
        }
    }
    
    func setupTableView() {
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshControlValueChanged), for: .valueChanged)
        tableView.emptyDataSetSource = self
        tableView.register(cellType: RepositoryTableViewCell.self)
    }
    
    @objc func refreshControlValueChanged() {
        presenter.refreshRepositories()
    }
    
    // MARK: - Repositories view protocol

    func setLoading(_ loading: Bool) {
        isLoading = loading
        if loading {
            activityIndicator.startAnimating()
        } else {
            refreshControl?.endRefreshing()
            activityIndicator.stopAnimating()
        }
    }
    
    func setEmpty(_ empty: Bool) {
        isEmpty = empty
        tableView.reloadEmptyDataSet()
    }
    
    func display(error: Error) {
        let alert = UIAlertController(title: NSLocalizedString("Error", comment: "Error title"), message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(.ok)
        present(alert, animated: true, completion: nil)
    }
    
    func insertRepositories(inRange range: CountableRange<Int>) {
        let newIndexPaths = range.map { IndexPath(row: $0, section: 0) }
        tableView.insertRows(at: newIndexPaths, with: .automatic)
    }
    
    func refreshRepositories() {
        tableView.reloadData()
    }
    
    func showRepositoryPullRequests(fromURL url: URL) {
        performSegue(withIdentifier: "showPullRequests", sender: url)
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRepositories
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: RepositoryTableViewCell.self)
        cell.setup(with: presenter.repository(atIndex: indexPath.row))
        return cell
    }
    
    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRepository(atIndex: indexPath.row)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPullRequests" {
            let navigationController = segue.destination as! UINavigationController
            let pullRequestsViewController = navigationController.viewControllers[0] as! PullRequestsTableViewController
            let url = sender as! URL
            pullRequestsViewController.repositoryPullRequests = url
        }
    }
    
    // MARK: - Scroll view delegate
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let reachedTheBottom = scrollView.contentOffset.y + scrollView.frame.size.height > scrollView.contentSize.height
        if reachedTheBottom && !isLoading {
            presenter.fetchNextRepositoriesPage()
        }
    }
    
    // MARK: - Empty data source
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let title = isEmpty ? NSLocalizedString("No repositories found", comment: "No repositories found message") : ""
        return NSAttributedString(string: title)
    }
    
    // MARK: - View controller previewing delegate
    
    @available(iOS 9.0, *)
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let indexPath = tableView.indexPathForRow(at: location), let sourceRect = tableView.cellForRow(at: indexPath)?.frame else {
            return nil
        }
        
        previewingContext.sourceRect = sourceRect
        
        let pullRequestsViewController = storyboard?.instantiateViewController(withIdentifier: "PullRequestsTableViewController") as? PullRequestsTableViewController
        pullRequestsViewController?.repositoryPullRequests = presenter.pullRequestsURLForRepository(at: indexPath.row)
        
        return pullRequestsViewController
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        navigationController?.show(viewControllerToCommit, sender: nil)
    }
    
    
}
