//
//  User.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 6/27/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import Foundation

struct User: Codable {
    
    var username: String
    var avatarURL: String?
    
    enum CodingKeys: String, CodingKey {
        case username = "login",
        avatarURL = "avatar_url"
    }
    
}
