//
//  RepositoryTableViewCell.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 1/20/18.
//  Copyright © 2018 Leonardo Oliveira. All rights reserved.
//

import UIKit
import Reusable

final class RepositoryTableViewCell: UITableViewCell, NibReusable {
    
    @IBOutlet weak var repositoryNameLabel: UILabel!
    @IBOutlet weak var repositoryDescriptionLabel: UILabel!
    @IBOutlet weak var forkCountLabel: UILabel!
    @IBOutlet weak var starCountLabel: UILabel!
    @IBOutlet weak var ownerPhotoImageView: RemoteImageView!
    @IBOutlet weak var ownerUsernameLabel: UILabel!
    
    func setup(with repository: RepositoryDataView) {
        repositoryNameLabel.text = repository.name
        repositoryDescriptionLabel.text = repository.description
        forkCountLabel.text = repository.numberOfForks
        starCountLabel.text = repository.numberOfStars
        ownerUsernameLabel.text = repository.ownerUsername
        ownerPhotoImageView.setImage(withURL: repository.ownerAvatarURL, placeholderImage: #imageLiteral(resourceName: "Photo Outline 50h"))
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        ownerPhotoImageView.cancelImageRequest()
    }
    
}
