//
//  PullRequestsPresenterProtocol.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 6/28/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import Foundation

protocol PullRequestsPresenterProtocol {
    weak var delegate: PullRequestsViewDelegate? { get set }
    var numberOfPullRequests: Int { get }
    func refreshPullRequests()
    func fetchNextPullRequestsPage()
    func pullRequest(atIndex index: Int) -> PullRequestDataView
    func didSelectPullRequest(atIndex index: Int)
    func pullRequestURL(at index: Int) -> URL?
}
