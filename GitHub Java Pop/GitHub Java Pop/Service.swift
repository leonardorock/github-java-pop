//
//  Service.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 1/20/18.
//  Copyright © 2018 Leonardo Oliveira. All rights reserved.
//

import Foundation

protocol Service {
    var session: URLSession { get }
    var decoder: JSONDecoder { get }
}

extension Service {
    
    typealias ModelResponse<T> = (success: (T) -> Void, failure: (Error) -> Void, completion: () -> Void)
    
    var session: URLSession {
        return URLSession.shared
    }
    
    var decoder: JSONDecoder {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZ"
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        return decoder
    }
    
    func request<T: Codable>(url: URLConvertible, parameters: [String : Any], response: ModelResponse<T>) {
        guard let url = url.asURL(), var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
            response.completion()
            return
        }
        let queryItems = parameters.map { key, value in URLQueryItem(name: key, value: "\(value)") }
        urlComponents.queryItems = queryItems
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = "GET"
        session.dataTask(with: request) { (data, urlResponse, error) in
            self.handleModelResponse(data: data, urlResponse: urlResponse, decoding: T.self, error: error, response: response)
        }.resume()
    }
    
    func handleModelResponse<T>(data: Data?, urlResponse: URLResponse?, decoding: T.Type, error: Error?, response: ModelResponse<T>) where T : Codable {
        DispatchQueue.main.async {
            let result = self.resolveResponse(data: data, urlResponse: urlResponse, decoding: decoding, error: error)
            self.buildResponse(response: response, result: result)
        }
    }
    
    private func buildResponse<T>(response: ModelResponse<T>, result: (parsed: T?, error: Error?)) {
        if let parsed = result.parsed {
            response.success(parsed)
        } else if let error = result.error {
            response.failure(error)
        }
        response.completion()
    }
    
    private func resolveResponse<T>(data: Data?, urlResponse: URLResponse?, decoding type: T.Type, error: Error?) -> (parsed: T?, error: Error?) where T : Codable {
        var result: T? = nil
        var error: Error? = error
        if let urlResponse = urlResponse as? HTTPURLResponse {
            if (200..<300).contains(urlResponse.statusCode), let data = data {
                do {
                    result = try decoder.decode(type, from: data)
                } catch let e {
                    error = e
                }
            } else {
                error = ServiceError(statusCode: urlResponse.statusCode, data: data)
            }
        }
        return (result, error)
    }

}
