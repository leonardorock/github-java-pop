//
//  PullRequestsViewProtocol.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 6/28/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import Foundation

protocol PullRequestsViewDelegate: class {
    var isLoading: Bool { get set }
    var isEmpty: Bool { get set }
    func setLoading(_ loading: Bool)
    func setEmpty(_ empty: Bool)
    func display(error: Error)
    func insertPullRequests(inRange range: CountableRange<Int>)
    func refreshPullRequests()
    func repositoryPullRequestsURL() -> URL?
    func openPullRequestPage(withURL url: URL)
}
