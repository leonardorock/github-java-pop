//
//  PullRequestDataView.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 6/28/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import UIKit

struct PullRequestDataView {
    
    var pullRequest: PullRequest
    
    var title: String? {
        return pullRequest.title
    }
    
    var subject: String? {
        return pullRequest.subject
    }
    
    var username: String? {
        return pullRequest.user?.username
    }
    
    var state: String? {
        return pullRequest.state?.rawValue
    }
    
    var stateColor: UIColor {
        guard let state = pullRequest.state else { return UIColor.orange }
        switch state {
        case .open: return #colorLiteral(red: 0.2666666667, green: 0.8588235294, blue: 0.368627451, alpha: 1)
        case .closed: return #colorLiteral(red: 0.9960784314, green: 0.2196078431, blue: 0.1411764706, alpha: 1)
        }
    }
    
    var date: String? {
        guard let date = pullRequest.date else {
            return nil
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        return dateFormatter.string(from: date)
    }
    
    var avatarURL: URL? {
        return pullRequest.user?.avatarURL?.asURL()
    }
}
