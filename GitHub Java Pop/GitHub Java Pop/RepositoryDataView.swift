//
//  RepositoryDataView.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 6/27/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import Foundation

struct RepositoryDataView {
    
    var repository: Repository
    
    var name: String? {
        return repository.name
    }
    
    var description: String? {
        return repository.description
    }
    
    var numberOfForks: String {
        return "\(repository.numberOfForks)"
    }
    
    var numberOfStars: String {
        return "\(repository.numberOfStars)"
    }
    
    var ownerUsername: String {
        return "\(repository.owner.username)"
    }
    
    var ownerAvatarURL: URL? {
        return repository.owner.avatarURL?.asURL()
    }
}
