//
//  RepositoryService.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 6/27/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import Foundation

class RepositoryService: Service {
    
    func fetchRepositories(for page: Int, response: ModelResponse<RepositoriesResponse>) {
        let url = "https://api.github.com/search/repositories"
        
        var parameters: [String : Any] = [:]
        parameters["q"] = "language:Java"
        parameters["sort"] = "stars"
        parameters["page"] = page
        
        request(url: url, parameters: parameters, response: response)
    }
    
}
