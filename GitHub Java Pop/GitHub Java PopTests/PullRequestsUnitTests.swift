//
//  PullRequestsUnitTests.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 6/28/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import XCTest
@testable import GitHub_Java_Pop

class PullRequestsUnitTests: XCTestCase {
    
    let user = User(username: "Leo", avatarURL: nil)
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPullRequestsRefreshAndNextPageFetch() {
        let pullRequest = PullRequest(title: "Cool pull", subject: "lorem ipsum", user: user, date: Date(), url: "https://github.com/ReactiveX/RxJava/pull/5447", state: .open)
        let service = PullRequestServiceMock(repositories: [pullRequest], error: nil)
        let view = PullRequestsViewMock()
        let presenterUnderTest = PullRequestsPresenter(service: service)
        presenterUnderTest.delegate = view
        
        presenterUnderTest.refreshPullRequests()
        XCTAssert(view.insertPullRequestsCalled, "refresh insert failed")
        XCTAssert(view.refreshPullRequestsCalled, "refresh view not called")
        
        view.insertPullRequestsCalled = false
        view.refreshPullRequestsCalled = false
        
        presenterUnderTest.fetchNextPullRequestsPage()
        XCTAssert(view.insertPullRequestsCalled, "next page call failed")
        XCTAssertFalse(view.refreshPullRequestsCalled, "refresh called on next page call")
    }
    
    func testPullRequestsRefreshAndNextPageError() {
        let error = NSError(domain: "Some error", code: 123, userInfo: nil)
        let service = PullRequestServiceMock(repositories: nil, error: error)
        let view = PullRequestsViewMock()
        let presenterUnderTest = PullRequestsPresenter(service: service)
        presenterUnderTest.delegate = view
        
        presenterUnderTest.refreshPullRequests()
        XCTAssert(view.displayErrorCalled, "refresh error message not shown")
        XCTAssert(view.refreshPullRequestsCalled, "refresh view not called")
        
        view.displayErrorCalled = false
        view.refreshPullRequestsCalled = false
        
        presenterUnderTest.fetchNextPullRequestsPage()
        XCTAssert(view.displayErrorCalled, "next page error message not shown")
        XCTAssertFalse(view.refreshPullRequestsCalled, "refresh called on next page call")
    }
    
    func testPullRequestsRefreshAndNextPageEmpty() {
        let service = PullRequestServiceMock(repositories: [], error: nil)
        let view = PullRequestsViewMock()
        let presenterUnderTest = PullRequestsPresenter(service: service)
        presenterUnderTest.delegate = view
        
        presenterUnderTest.refreshPullRequests()
        XCTAssert(view.isEmpty, "refresh empty state not set")
        XCTAssert(view.refreshPullRequestsCalled, "refresh view not called")
        
        view.isEmpty = false
        view.refreshPullRequestsCalled = false
        
        presenterUnderTest.fetchNextPullRequestsPage()
        XCTAssert(view.isEmpty, "next page empty state not set")
        XCTAssertFalse(view.refreshPullRequestsCalled, "refresh called on next page call")
    }
    
    func testPullRequestSelected() {
        let pullRequest = PullRequest(title: "Cool pull", subject: "lorem ipsum", user: user, date: Date(), url: "https://github.com/ReactiveX/RxJava/pull/5447", state: .open)
        let service = PullRequestServiceMock(repositories: [pullRequest], error: nil)
        let view = PullRequestsViewMock()
        let presenterUnderTest = PullRequestsPresenter(service: service)
        presenterUnderTest.delegate = view
        
        presenterUnderTest.refreshPullRequests()
        XCTAssert(view.insertPullRequestsCalled, "refresh insert failed")
        XCTAssert(view.refreshPullRequestsCalled, "refresh view not called")
        
        presenterUnderTest.didSelectPullRequest(atIndex: 0)
        XCTAssert(view.openPullRequestPageCalled, "open url not called")
    }
    
}
