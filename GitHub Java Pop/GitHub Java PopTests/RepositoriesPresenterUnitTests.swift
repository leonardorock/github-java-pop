//
//  RepositoriesPresenterUnitTests.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 6/28/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import XCTest
@testable import GitHub_Java_Pop

class RepositoriesPresenterUnitTests: XCTestCase {
    
    let user = User(username: "Leo", avatarURL: nil)
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testRepositoriesRefreshAndNextPageFetch() {
        let repository = Repository(name: "Cool rep", description: "another rep", numberOfForks: 12, numberOfStars: 42, owner: user, pullRequestUrl: "https://api.github.com/repos/ReactiveX/RxJava/pulls")
        let repositoriesResponse = RepositoriesResponse(numberOfRepositories: 1, incompleteResults: false, repositories: [repository])
        let service = RepositoryServiceMock(repositoriesResponse: repositoriesResponse, error: nil)
        let view = RepositoriesViewMock()
        let presenterUnderTest = RepositoriesPresenter(service: service)
        presenterUnderTest.delegate = view
        
        presenterUnderTest.refreshRepositories()
        XCTAssert(view.insertRepositoriesCalled, "refresh insert failed")
        XCTAssert(view.refreshRepositoriesCalled, "refresh view not called")
        
        view.insertRepositoriesCalled = false
        view.refreshRepositoriesCalled = false
        
        presenterUnderTest.fetchNextRepositoriesPage()
        XCTAssert(view.insertRepositoriesCalled, "next page call failed")
        XCTAssertFalse(view.refreshRepositoriesCalled, "refresh called on next page call")
    }
    
    func testRepositoriesRefreshAndNextPageError() {
        let error = NSError(domain: "Error", code: 123, userInfo: nil)
        let service = RepositoryServiceMock(repositoriesResponse: nil, error: error)
        let view = RepositoriesViewMock()
        let presenterUnderTest = RepositoriesPresenter(service: service)
        presenterUnderTest.delegate = nil
        
        presenterUnderTest.refreshRepositories()
        XCTAssert(view.displayErrorCalled, "refresh error message not shown")
        XCTAssert(view.refreshRepositoriesCalled, "refresh view not called")
        
        view.displayErrorCalled = false
        view.refreshRepositoriesCalled = false
        
        presenterUnderTest.fetchNextRepositoriesPage()
        XCTAssert(view.displayErrorCalled, "next page error message not shown")
        XCTAssertFalse(view.refreshRepositoriesCalled, "refresh called on next page call")
    }
    
    func testRepositoriesRefreshAndNextPageEmpty() {
        let repositoriesResponse = RepositoriesResponse(numberOfRepositories: 0, incompleteResults: false, repositories: [])
        let service = RepositoryServiceMock(repositoriesResponse: repositoriesResponse, error: nil)
        let view = RepositoriesViewMock()
        let presenterUnderTest = RepositoriesPresenter(service: service)
        presenterUnderTest.delegate = view
        
        presenterUnderTest.refreshRepositories()
        XCTAssert(view.isEmpty, "refresh empty state not set")
        XCTAssert(view.refreshRepositoriesCalled, "refresh view not called")
        
        view.isEmpty = false
        view.refreshRepositoriesCalled = false
        
        presenterUnderTest.fetchNextRepositoriesPage()
        XCTAssert(view.isEmpty, "next page empty state not set")
        XCTAssertFalse(view.refreshRepositoriesCalled, "refresh called on next page call")
    }
    
    func testRepositorySelected() {
        let repository = Repository(name: "Cool rep", description: "another rep", numberOfForks: 12, numberOfStars: 42, owner: user, pullRequestUrl: "https://api.github.com/repos/ReactiveX/RxJava/pulls")
        let repositoriesResponse = RepositoriesResponse(numberOfRepositories: 1, incompleteResults: false, repositories: [repository])
        let service = RepositoryServiceMock(repositoriesResponse: repositoriesResponse, error: nil)
        let view = RepositoriesViewMock()
        let presenterUnderTest = RepositoriesPresenter(service: service)
        presenterUnderTest.delegate = view
        
        presenterUnderTest.fetchNextRepositoriesPage()
        XCTAssert(view.insertRepositoriesCalled)
        
        presenterUnderTest.didSelectRepository(atIndex: 0)
        XCTAssert(view.showRepositoryPullRequestsCalled)
    }
    
}
