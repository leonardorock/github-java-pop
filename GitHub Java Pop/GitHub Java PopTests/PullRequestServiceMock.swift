//
//  PullRequestServiceMock.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 6/28/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import Foundation
@testable import GitHub_Java_Pop

class PullRequestServiceMock: PullRequestService {
    var pullRequests: [PullRequest]?
    var error: Error?
    
    init(repositories: [PullRequest]?, error: Error?) {
        self.pullRequests = repositories
        self.error = error
    }
    
    override func fetchPullRequests(at url: URL, page: Int, response: (success: ([PullRequest]) -> Void, failure: (Error) -> Void, completion: () -> Void)) {
        if let pullRequests = pullRequests {
            response.success(pullRequests)
        } else if let error = error {
            response.failure(error)
        }
        response.completion()
    }
    
}
