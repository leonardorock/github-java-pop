//
//  PullRequestsViewMock.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 6/28/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import Foundation
@testable import GitHub_Java_Pop

class PullRequestsViewMock: PullRequestsViewDelegate {
    var isLoading: Bool = false
    var isEmpty: Bool = false
    
    // call checks
    var displayErrorCalled: Bool = false
    var insertPullRequestsCalled: Bool = false
    var refreshPullRequestsCalled: Bool = false
    var repositoryPullRequestsURLCalled: Bool = false
    var openPullRequestPageCalled: Bool = false
    
    func setLoading(_ loading: Bool) {
        isLoading = loading
    }
    
    func setEmpty(_ empty: Bool) {
        isEmpty = empty
    }
    
    func display(error: Error) {
        displayErrorCalled = true
    }
    
    func insertPullRequests(inRange range: CountableRange<Int>) {
        insertPullRequestsCalled = true
    }
    
    func refreshPullRequests() {
        refreshPullRequestsCalled = true
    }
    
    func repositoryPullRequestsURL() -> URL? {
        repositoryPullRequestsURLCalled = true
        return URL(string: "https://api.github.com/repos/ReactiveX/RxJava/pulls")
    }
    
    func openPullRequestPage(withURL url: URL) {
        openPullRequestPageCalled = true
    }
}
