//
//  RepositoryServiceMock.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 6/28/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

@testable import GitHub_Java_Pop

class RepositoryServiceMock: RepositoryService {
    var repositoriesResponse: RepositoriesResponse?
    var error: Error?
    
    init(repositoriesResponse: RepositoriesResponse?, error: Error?) {
        self.repositoriesResponse = repositoriesResponse
        self.error = error
    }
    
    override func fetchRepositories(for page: Int, response: (success: (RepositoriesResponse) -> Void, failure: (Error) -> Void, completion: () -> Void)) {
        if let repositoriesResponse = repositoriesResponse {
            response.success(repositoriesResponse)
        } else if let error = error {
            response.failure(error)
        }
        response.completion()
    }
    
}
