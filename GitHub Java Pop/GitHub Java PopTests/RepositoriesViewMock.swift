//
//  RepositoriesViewMock.swift
//  GitHub Java Pop
//
//  Created by Leonardo Oliveira on 6/28/17.
//  Copyright © 2017 Leonardo Oliveira. All rights reserved.
//

import Foundation
@testable import GitHub_Java_Pop

class RepositoriesViewMock: RepositoriesViewDelegate {
    
    var isLoading: Bool = false
    var isEmpty: Bool = false
    
    // call checks
    var displayErrorCalled: Bool = false
    var insertRepositoriesCalled: Bool = false
    var refreshRepositoriesCalled: Bool = false
    var showRepositoryPullRequestsCalled: Bool = false
    
    func setLoading(_ loading: Bool) {
        isLoading = loading
    }
    
    func setEmpty(_ empty: Bool) {
        isEmpty = empty
    }
    
    func display(error: Error) {
        displayErrorCalled = true
    }
    
    func insertRepositories(inRange range: CountableRange<Int>) {
        insertRepositoriesCalled = true
    }
    
    func refreshRepositories() {
        refreshRepositoriesCalled = true
    }
    
    func showRepositoryPullRequests(fromURL url: URL) {
        showRepositoryPullRequestsCalled = true
    }
    
}
